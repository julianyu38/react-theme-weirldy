import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import Tree from '../../../routes/components/dataDisplay/Tree'

export default Page(() => (
  <>
    <Head>
      <title>Tree</title>
    </Head>
    <>
      <Tree/>
    </>
  </>
));