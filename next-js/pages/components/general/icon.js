import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import Icon from '../../../routes/components/general/Icon'

export default Page(() => (
  <>
    <Head>
      <title>Icon</title>
    </Head>
    <>
      <Icon/>
    </>
  </>
));