import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import Upload from '../../../routes/components/dataEntry/Upload'

export default Page(() => (
  <>
    <Head>
      <title>Upload</title>
    </Head>
    <>
      <Upload/>
    </>
  </>
));