import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import Slider from '../../../routes/components/dataEntry/Slider'

export default Page(() => (
  <>
    <Head>
      <title>Slider</title>
    </Head>
    <>
      <Slider/>
    </>
  </>
));