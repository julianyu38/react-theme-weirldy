import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import InputNumber from '../../../routes/components/dataEntry/InputNumber'

export default Page(() => (
  <>
    <Head>
      <title>InputNumber</title>
    </Head>
    <>
      <InputNumber/>
    </>
  </>
));