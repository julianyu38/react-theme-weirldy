import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import Pagination from '../../../routes/components/navigation/Pagination'

export default Page(() => (
  <>
    <Head>
      <title>Pagination</title>
    </Head>
    <>
      <Pagination/>
    </>
  </>
));