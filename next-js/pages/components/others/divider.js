import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import Divider from '../../../routes/components/others/Divider'

export default Page(() => (
  <>
    <Head>
      <title>Divider</title>
    </Head>
    <>
      <Divider/>
    </>
  </>
));