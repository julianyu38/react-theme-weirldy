import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import PopConfirm from '../../../routes/components/feedback/PopConfirm'

export default Page(() => (
  <>
    <Head>
      <title>PopConfirm</title>
    </Head>
    <>
      <PopConfirm/>
    </>
  </>
));