import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import Notification from '../../../routes/components/feedback/Notification'

export default Page(() => (
  <>
    <Head>
      <title>Notification</title>
    </Head>
    <>
      <Notification/>
    </>
  </>
));