import Head from 'next/head';
import Page from '../../../hoc/securedPage';

import asyncComponent from "../../../util/asyncComponent";

const Listing = asyncComponent(() => import('../../../routes/main/Dashboard/Listing'));

export default Page(() => (
  <>
    <Head>
      <title>Listing Dashborad</title>
    </Head>
    <>
      <Listing/>
    </>
  </>
));