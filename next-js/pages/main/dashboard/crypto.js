import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import asyncComponent from "../../../util/asyncComponent";

const Crypto = asyncComponent(() => import('../../../routes/main/Dashboard/Crypto'));

export default Page(() => (
  <>
    <Head>
      <title>Crypto Dashborad</title>
    </Head>
    <>
      <Crypto/>
    </>
  </>
));