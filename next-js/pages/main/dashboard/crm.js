import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import asyncComponent from "../../../util/asyncComponent";

const CRM = asyncComponent(() => import('../../../routes/main/Dashboard/CRM'));

export default Page(() => (
  <>
    <Head>
      <title>CRM Dashborad</title>
    </Head>
    <>
      <CRM/>
    </>
  </>
));