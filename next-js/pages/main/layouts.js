import Head from 'next/head';
import Page from '../../hoc/securedPage';
import asyncComponent from "../../util/asyncComponent";

const Layouts = asyncComponent(() => import('../../routes/main/Layouts'));

export default Page(() => (
  <>
    <Head>
      <title>Layouts</title>
    </Head>
    <>
      <Layouts/>
    </>
  </>
));