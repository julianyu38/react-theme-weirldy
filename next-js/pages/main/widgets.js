import Head from 'next/head';
import Page from '../../hoc/securedPage';
import asyncComponent from "../../util/asyncComponent";
import CircularProgress from "../../components/CircularProgress";

const Widgets = asyncComponent(() => import('../../routes/main/Widgets'));

export default Page(() => (
  <>
    <Head>
      <title>Widgets</title>
    </Head>
    <>
      <CircularProgress/>
    </>
  </>
));