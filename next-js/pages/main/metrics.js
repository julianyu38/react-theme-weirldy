import Head from 'next/head';
import Page from '../../hoc/securedPage';
import asyncComponent from "../../util/asyncComponent";

const Metrics = asyncComponent(() => import('../../routes/main/Metrics'));

export default Page(() => (
  <>
    <Head>
      <title>Metrics</title>
    </Head>
    <>
      <Metrics/>
    </>
  </>
));