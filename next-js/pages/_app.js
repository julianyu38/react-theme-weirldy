import React from 'react';
import App, {Container} from 'next/app';
import Head from 'next/head'
import {Provider} from 'react-redux';
import withRedux from 'next-redux-wrapper';
import "../static/vendors/style";
import "../styles/style.min.css"
import "../firebaseConfig/index"

import initStore from '../redux/store';

class MyApp extends App {
  static async getInitialProps({Component, router, ctx}) {
    return {
      pageProps: {
        ...(Component.getInitialProps
          ? await Component.getInitialProps(ctx)
          : {}),
      },
    };
  }

  render() {
    const {Component, pageProps, store} = this.props;
    return (
      <Container>
        <Head>
          <title>Wieldy- Admin Dashboard</title>
        </Head>
        <Container>
          <Provider store={store}>
            <Component {...pageProps} />
          </Provider>
        </Container>
      </Container>
    );
  }
}

export default withRedux(initStore)(MyApp);
