import Head from 'next/head';
import Page from '../../../../hoc/securedPage';
import Area from '../../../../routes/extensions/charts/recharts/Area'

export default Page(() => (
  <>
    <Head>
      <title>Area Chart</title>
    </Head>
    <>
      <Area/>
    </>
  </>
));