import Head from 'next/head';
import Page from '../../../../hoc/securedPage';
import Radar from '../../../../routes/extensions/charts/recharts/Radar'

export default Page(() => (
  <>
    <Head>
      <title>Radar Chart</title>
    </Head>
    <>
      <Radar/>
    </>
  </>
));