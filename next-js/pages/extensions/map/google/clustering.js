import Head from 'next/head';
import Page from '../../../../hoc/securedPage';
import Clustering from '../../../../routes/extensions/map/googlemap/Clustering'

export default Page(() => (
  <>
    <Head>
      <title>Google Map - Clustering</title>
    </Head>
    <>
      <Clustering/>
    </>
  </>
));