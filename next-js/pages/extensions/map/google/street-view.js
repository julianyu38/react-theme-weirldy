import Head from 'next/head';
import Page from '../../../../hoc/securedPage';
import StreetView from '../../../../routes/extensions/map/googlemap/StreetView'

export default Page(() => (
  <>
    <Head>
      <title>Google Map - StreetView</title>
    </Head>
    <>
      <StreetView/>
    </>
  </>
));