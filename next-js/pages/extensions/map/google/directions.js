import Head from 'next/head';
import Page from '../../../../hoc/securedPage';
import Directions from '../../../../routes/extensions/map/googlemap/Directions'

export default Page(() => (
  <>
    <Head>
      <title>Google Map - Directions</title>
    </Head>
    <>
      <Directions/>
    </>
  </>
));