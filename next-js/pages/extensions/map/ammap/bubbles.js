import Head from 'next/head';
import Page from '../../../../hoc/securedPage';
import Bubbles from '../../../../routes/extensions/map/ammap/Bubbles'

export default Page(() => (
  <>
    <Head>
      <title>Ammap-Bubbles</title>
    </Head>
    <>
      <Bubbles/>
    </>
  </>
));