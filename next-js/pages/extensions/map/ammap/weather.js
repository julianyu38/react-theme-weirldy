import Head from 'next/head';
import Page from '../../../../hoc/securedPage';
import Weather from '../../../../routes/extensions/map/ammap/Weather'

export default Page(() => (
  <>
    <Head>
      <title>Ammap-Weather</title>
    </Head>
    <>
      <Weather/>
    </>
  </>
));