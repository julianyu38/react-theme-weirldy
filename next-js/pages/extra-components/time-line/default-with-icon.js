import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import Default from '../../../routes/extraComponents/timeLine/DefaultWithIcon'

export default Page(() => (
  <>
    <Head>
      <title>Default With Icon Timeline</title>
    </Head>
    <>
      <Default/>
    </>
  </>
));