import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import LeftAligned from '../../../routes/extraComponents/timeLine/LeftAligned'

export default Page(() => (
  <>
    <Head>
      <title>Left Aligned Timeline</title>
    </Head>
    <>
      <LeftAligned/>
    </>
  </>
));