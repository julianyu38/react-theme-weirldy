import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import WYSISWYG from '../../../routes/extraComponents/editors/WYSISWYG'

export default Page(() => (
  <>
    <Head>
      <title>WYSISWYG Editor</title>
    </Head>
    <>
      <WYSISWYG/>
    </>
  </>
));