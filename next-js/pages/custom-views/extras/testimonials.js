import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import Testimonials from '../../../routes/customViews/extras/Testimonials'

export default Page(() => (
  <>
    <Head>
      <title>Testimonials</title>
    </Head>
    <>
      <Testimonials/>
    </>
  </>
));