import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import PricingTable from '../../../routes/customViews/extras/PricingTable'

export default Page(() => (
  <>
    <Head>
      <title>Pricing Table</title>
    </Head>
    <>
      <PricingTable/>
    </>
  </>
));