import Head from 'next/head';
import Page from '../../../hoc/securedPage';
import SignIn from '../../../routes/customViews/userAuth/SignIn'

export default Page(() => (
  <>
    <Head>
      <title>SignIn</title>
    </Head>
    <>
      <SignIn/>
    </>
  </>
));