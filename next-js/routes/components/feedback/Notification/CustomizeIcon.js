import React from "react";
import {Button, Card, Icon, notification} from "antd";

const openNotification = () => {
  notification.open({
    message: 'notification Title',
    description: 'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
    icon: <Icon type="smile-circle" style={{color: '#108ee9'}}/>,
  });
};

const CustomizeIcon = () => {
  return (
    <Card title="Customize Icon" className="gx-card">
      <Button type="primary" onClick={openNotification}>Open the notification box</Button>
    </Card>
  );
};

export default CustomizeIcon;
