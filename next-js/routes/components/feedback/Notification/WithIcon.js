import React from "react";
import {Button, Card, notification} from "antd";

const openNotificationWithIcon = (type) => {
  notification[type]({
    message: 'notification Title',
    description: 'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
  });
};

const WithIcon = () => {
  return (
    <Card title="With Icon" className="gx-card">
      <Button onClick={() => openNotificationWithIcon('success')}>Success</Button>
      <Button onClick={() => openNotificationWithIcon('info')}>Info</Button>
      <Button onClick={() => openNotificationWithIcon('warning')}>Warning</Button>
      <Button onClick={() => openNotificationWithIcon('error')}>Error</Button>
    </Card>
  );
};

export default WithIcon;
