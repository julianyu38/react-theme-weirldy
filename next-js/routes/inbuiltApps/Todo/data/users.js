export default [
  {
    'id': 1,
    'name': 'Alex Dolgove',
    'thumb': '../../../static//images/avatar/stella-johnson.png',
  }, {
    'id': 2,
    'name': 'Domnic Harris',
    'thumb': '../../../static//images/avatar/domnic-harris.png',
  }, {
    'id': 3,
    'name': 'Garry Sobars',
    'thumb': '../../../static//images/avatar/garry-sobars.png',
  }, {
    'id': 4,
    'name': 'Stella Johnson',
    'thumb': '../../../static//images/avatar/stella-johnson.png',
  }, {
    'id': 5,
    'name': 'John Smith',
    'thumb': '../../../static//images/avatar/john-smith.png',
  }, {
    'id': 6,
    'name': 'Domnic Brown',
    'thumb': '../../../static//images/avatar/domnic-brown.png',
  }
]
